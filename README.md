# guide-idea-plugin-orm



## 项目介绍
插件通过jdbc连接数据库，复选框勾选需要生成映射文件的表并选择文件生成目录后，点击创建，就可以生成文件。

开发时参考了这篇文章：
https://bugstack.cn/md/assembly/idea-plugin/2021-08-27-%E6%8A%80%E6%9C%AF%E8%B0%83%E7%A0%94%EF%BC%8CIDEA%20%E6%8F%92%E4%BB%B6%E6%80%8E%E4%B9%88%E5%BC%80%E5%8F%91%EF%BC%9F.html
