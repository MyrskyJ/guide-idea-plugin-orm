package cn.bugstack.guide.idea.plugin.infrastructure.po;

import cn.bugstack.guide.idea.plugin.infrastructure.utils.JavaType;
import com.google.common.base.CaseFormat;
import org.apache.commons.lang3.StringUtils;

/**
 * @author: 小傅哥，微信：fustack
 * @github: https://github.com/fuzhengwei
 * @Copyright: 公众号：bugstack虫洞栈 | 博客：https://bugstack.cn - 沉淀、分享、成长，让自己和他人都能有所收获！
 */
public class Field {

    private String comment;
    private String columnName;
    private Class<?> type;
    private boolean id;
    private boolean isNotBaseFieldFlag = true;

    public Field(String comment, Class<?> type, String columnName) {
        this.comment = comment;
        this.type = type;
        this.columnName = columnName;
        this.isNotBaseFieldFlag = isNotBaseField();
    }

    public String getComment() {
        return comment;
    }

    public String getTypeName() {
        return type.getName();
    }

    public String getTypeSimpleName() {
        return type.getSimpleName();
    }

    public String getColumnName() {
        return columnName;
    }

    public Class<?> getType() {
        return type;
    }

    public boolean getIsNotBaseFieldFlag() {
        return isNotBaseFieldFlag;
    }

    public String getName() {
        String str = columnName;
        if (StringUtils.startsWithIgnoreCase(str, "create_by")
                || StringUtils.startsWithIgnoreCase(str, "update_by")){
            //column.setJavaType(User.class.getName());
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, str) + ".id";
        }
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, str);
    }

    public void setId(boolean id) {
        this.id = id;
    }

    public boolean isId() {
        return id;
    }

    public boolean isImport() {
        String typeName = getTypeName();
        return !type.isPrimitive() && !"java.lang".equals(StringUtils.substringBeforeLast(typeName, "."));
    }

    public boolean isNotBaseField() {
        String str = columnName;
        if (StringUtils.startsWithIgnoreCase(str, "create_by")
                || StringUtils.startsWithIgnoreCase(str, "update_by")
                || StringUtils.startsWithIgnoreCase(str, "id")
                || StringUtils.startsWithIgnoreCase(str, "update_date")
                || StringUtils.startsWithIgnoreCase(str, "del_flag")
                || StringUtils.startsWithIgnoreCase(str, "create_date")){
            return false;
        }
        return true;
    }

}
