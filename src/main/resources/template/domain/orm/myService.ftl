/**
 * Copyright &copy; 2012-2014 <a href="http://www.jeesz.com">Jeesz</a> All rights reserved.
 */
package ;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sml.sz.common.persistence.Page;
import com.sml.sz.common.service.CrudService;
import com.sml.sz.StringUtils;
<#list imports as import>
import ${import};
</#list>

/**
 * ${comment}Service
 */
@Service
public class ${simpleName} extends CrudService<${dao.simpleName}, ${model.simpleName}> {

	public ${model.simpleName} get(String id) {
		return super.get(id);
	}
	
	public List<${model.simpleName}> findList(${model.simpleName} ${model.varName}) {
		return super.findList(${model.varName});
	}
	
	public Page<${model.simpleName}> findPage(Page<${model.simpleName}> page, ${model.simpleName} ${model.varName}) {
		return super.findPage(page, ${model.varName});
	}
	
	public void save(${model.simpleName} ${model.varName}) {
		super.save(${model.varName});
	}
	
	public void delete(${model.simpleName} ${model.varName}) {
		super.delete(${model.varName});
	}
	
}