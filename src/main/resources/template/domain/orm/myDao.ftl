/**
* Copyright &copy; 2012-2014 <a href="http://www.jeesz.com">Jeesz</a> All rights reserved.
*/
package ${package};

import com.sml.sz.common.persistence.CrudDao;
import com.sml.sz.common.persistence.annotation.MyBatisDao;
<#list imports as import>
import ${import};
</#list>

/**
* ${comment}DAO接口
*/
@MyBatisDao
public interface ${simpleName} extends CrudDao<${model.simpleName}> {

}