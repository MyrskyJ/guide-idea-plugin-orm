/**
* Copyright &copy; 2012-2014 <a href="http://www.jeesz.com">Jeesz</a> All rights reserved.
*/
package ${package};

<#list imports as import>
import ${import};
</#list>

import com.sml.sz.common.persistence.DataEntity;

/**
* ${comment}Entity
*/
public class ${simpleName} extends DataEntity<${simpleName}> {

    private static final long serialVersionUID = 1L;
<#-- 生成字段属性 -->
<#list fields as field>
<#-- 如果不是基类属性 -->
    <#if field.isNotBaseFieldFlag>
        <#if field.comment??>
    /**
    * ${field.comment}
    */
        </#if>
    private ${field.typeSimpleName} ${field.name};
    </#if>
</#list>

    <#-- 构造方法 -->
    public ${simpleName}() {
        super();
    }

    public ${simpleName}(String id){
        super(id);
    }

<#-- 生成get和set方法 -->
<#list fields as field>
<#-- 如果不是基类属性 -->
    <#if field.isNotBaseFieldFlag>
    public ${field.typeSimpleName} get${field.name?cap_first}() {
        return ${field.name};
    }

    public void set${field.name?cap_first}(${field.typeSimpleName} ${field.name}) {
        this.${field.name} = ${field.name};
     }
    </#if>
</#list>
}