<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${package}.${simpleName}">

    <#-- 输出字段列 -->
    <sql id="${model.varName}Columns">
        <#assign columnField>
            <#list model.fields as field>
        a.${field.columnName} AS "${field.name}",
            </#list>
        </#assign>
        ${columnField?substring(0, columnField?last_index_of(","))}
    </sql>

    <#-- 输出字段关联表 -->
    <sql id="${model.varName}Joins">
    </sql>

    <select id="get" resultType="${model.package}.${model.simpleName}">
        SELECT
        <include refid="${model.varName}Columns"/>
        FROM ${model.tableName} a
        <include refid="${model.varName}Joins"/>
        WHERE a.id = ${"#"}{id}
    </select>

    <select id="findList" resultType="${model.package}.${model.simpleName}">
        SELECT
        <include refid="${model.varName}Columns"/>
        FROM ${model.tableName} a
        <include refid="${model.varName}Joins"/>
        <where>
            a.del_flag = ${"#"}{DEL_FLAG_NORMAL}
            <#list model.fields as field>
            <if test="${field.name} != null and ${field.name} != ''">
                AND a.${field.columnName} = ${"#"}{${field.name}}
            </if>
            </#list>
        </where>
        <choose>
            <when test="page !=null and page.orderBy != null and page.orderBy != ''">
                ORDER BY ${"$"}{page.orderBy}
            </when>
            <otherwise>
                ORDER BY a.update_date DESC
            </otherwise>
        </choose>
    </select>

    <select id="findAllList" resultType="${model.package}.${model.simpleName}">
        SELECT
        <include refid="${model.varName}Columns"/>
        FROM ${model.tableName} a
        <include refid="${model.varName}Joins"/>
        <where>
            a.del_flag = ${"#"}{DEL_FLAG_NORMAL}
        </where>
        <choose>
            <when test="page !=null and page.orderBy != null and page.orderBy != ''">
                ORDER BY ${"$"}{page.orderBy}
            </when>
            <otherwise>
                ORDER BY a.update_date DESC
            </otherwise>
        </choose>
    </select>

    <insert id="insert">
        INSERT INTO ${model.tableName}(
        <#assign insertField>
            <#list model.fields as field>
            ${field.columnName},
            </#list>
        </#assign>
        ${insertField?substring(0, insertField?last_index_of(","))}
        ) VALUES (
        <#assign insertJavaField>
            <#list model.fields as field>
            ${"#"}{${field.name}},
            </#list>
        </#assign>
        ${insertJavaField?substring(0, insertJavaField?last_index_of(","))}
        )
    </insert>

    <update id="update">
        UPDATE ${model.tableName} SET
        <#assign updateField>
            <#list model.fields as field>
            ${field.columnName} = ${"#"}{${field.name}},
            </#list>
        </#assign>
        ${updateField?substring(0, updateField?last_index_of(","))}
        WHERE id = ${"#"}{id}
    </update>

    <update id="delete">
        UPDATE ${model.tableName} SET
        del_flag = ${"#"}{DEL_FLAG_DELETE}
        WHERE id = ${"#"}{id}
    </update>

</mapper>